const express = require('express');
const app = express();
const axios = require('axios');
const SlackBot = require('slackbots');
const slackToken = process.env.VERIFICATION_TOKEN;
const dotenv = require('dotenv')
dotenv.config();


/***
 * checking authorization
 * "scope": "admin.*"
 * https://slack.com/oauth/authorize
 */
authorize().catch(err => console.log(err));
async function authorize() {
    const url = 'https://slack.com/oauth/authorize';
    const res = await axios.get(url, {
        client_id: process.env.CLIENT_ID,
        scope: 'admin.*'
    }, { headers: { authorization: `Bearer ${slackToken}` } });

    console.log('Done', res.data);
}

/**
 * function for chat message
 * slackToken will check for authorization
 */
run().catch(err => console.log(err));
async function run() {
    const url = 'https://slack.com/api/chat.postMessage';
    const res = await axios.post(url, {
        channel: '#product',
        text: 'Hello, Shobhit'
    }, { headers: { authorization: `Bearer ${slackToken}` } });

    console.log('Done', res.data);
}
app.listen("3001", function (err, res) {
    console.log('sdfsfd')
});